#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int SIZE = 10;

void display_grid(int *grid) {
	int j, i;
	for (i=0; i < SIZE; i++) {
		for (j=0; j < SIZE; j++) { 
			printf("%2d ", grid[i*SIZE+j]);
		}
		printf("\n");
}}

int* generate_grid() {
	int i, j, k, valid, candidate, candidate_idx, max_attemps, tmp;

	srand(time(NULL));

	unsigned int enum_size = SIZE     *sizeof(int);
	unsigned int grid_size = SIZE*SIZE*sizeof(int);

	int *grid_data = (int *)(malloc(grid_size));
	int *enum_data = (int *)(malloc(enum_size));

	memset(grid_data, 0, grid_size);

	do{
		for(k=0;k < SIZE; k++) {
			for(i=0;i < SIZE; i++) {
				enum_data[i] = i+1;
			}

			for(i=0;i < SIZE; i++) {
				max_attemps   = SIZE-i-1;
				valid         = 1;
				candidate_idx = rand()%(SIZE-i);

			retry:
				candidate = enum_data[candidate_idx];
				for (j=0; j < SIZE; j++) {
					if(candidate == grid_data[j*SIZE+i]){
						if(max_attemps-- <= 0){
							valid = 0;
							break;
						} else {
							tmp                      = enum_data[candidate_idx];
							enum_data[candidate_idx] = enum_data[max_attemps  ];
							enum_data[max_attemps  ] = tmp;
							goto retry;
				}}}

				if(valid){
					enum_data[candidate_idx] = enum_data[SIZE-i-1];
					grid_data[k*SIZE+i]      = candidate;
				} else {
					k = i = j = -1;
					memset(grid_data, 0, grid_size);
					break;
	}}}} while(k < SIZE && valid);
	return grid_data;
}

int main(){
	display_grid(generate_grid());
	return 1;
}